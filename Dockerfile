FROM debian:bookworm

SHELL ["/bin/bash", "-c"]

LABEL org.opencontainers.artifact.description="Apache 2.4 (Debian/bookworm) with mod_auth_openidc and mod_xsendfile"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/devops/container/apache2-okd"
LABEL org.opencontainers.image.version="2.4"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.license="Apache-2.0"

ENV APACHE_LOG_LEVEL="info" \
    APACHE_OPT_CONF_DIR="/opt/apache2/conf" \
    APACHE_RUN_GROUP="root" \
    APACHE_SERVER_ADMIN="library-system-administration@duke.edu" \
    APACHE_SERVER_NAME="localhost" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    TZ="US/Eastern"

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install \
    apache2 \
    apache2-utils \
    curl \
    libapache2-mod-auth-openidc \
    libapache2-mod-xsendfile \
    ; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*

WORKDIR /etc/apache2

COPY ./etc/ ./
COPY ./bin/ /usr/local/bin/

RUN set -eux; \
    # Enable modules
    a2enmod headers info proxy proxy_connect proxy_http proxy_wstunnel rewrite; \
    # Disable default modules
    a2disconf other-vhosts-access-log serve-cgi-bin security; \
    # Enable custom global/server conf
    a2enconf global; \
    # Disable Debian default site
    a2dissite 000-default; \
    # Enable custom site on port 8080
    # a2ensite 8080-vhost; \
    chmod +x /usr/local/bin/start-fg; \
    # Custom conf dir for, e.g., mounting via ConfigMap
    mkdir -p $APACHE_OPT_CONF_DIR; \
    # N.B. We have to pre-create the PID dir in /var/run
    # and make it group writeable or Apache won't start.
    mkdir -m 0775 -p /opt/apache2/conf /var/run/apache2; \
    chmod -R g=u /etc/apache2 /var/run/apache2 /var/www/html $APACHE_OPT_CONF_DIR

VOLUME /var/www/html /opt/apache2/conf

# https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
STOPSIGNAL SIGWINCH

EXPOSE 8080

CMD [ "/usr/local/bin/start-fg" ]

USER www-data:root

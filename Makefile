SHELL = /bin/bash

build_tag ?= apache2-okd

.PHONY: build
build:
	docker build -t $(build_tag) .

.PHONY: test
test:
	build_tag=$(build_tag) ./test.sh

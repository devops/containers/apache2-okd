#!/bin/bash

echo "Starting container ..."
container="$(docker run --rm -d ${build_tag})"

code=1
SECONDS=0
while true; do
    echo "Checking status ..."
    if docker exec ${container} curl -sf -o /dev/null 'http://localhost:8080/server-status?auto'; then
	echo "SUCCESS"
	code=0
	break
    fi
    if [[ $SECONDS -gt 60 ]]; then
	break
    fi
    sleep 1
done

echo "Stopping container ..."
docker stop ${container} 2>/dev/null


echo "DONE"
exit $code
